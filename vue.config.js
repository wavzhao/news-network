module.exports = {
  // 配置跨域请求
  devServer: {
    port: 3001,
    https: false,
    proxy: {
      '/ug': {
        target: 'https://c.m.163.com',
        ws: true,
        changeOrigin: true,
      },
      '/api': {
        target: 'http://1.14.177.166:3007',
        ws: true,
        changeOrigin: true,
      },
      '/my': {
        target: 'http://1.14.177.166:3007',
        ws: true,
        changeOrigin: true,
      },
      '/artcate': {
        target: 'http://1.14.177.166:3007',
        ws: true,
        changeOrigin: true,
      }
    }
 
  }
 
}