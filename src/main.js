import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import service from './api/request'
import md5 from 'js-md5'

Vue.use(ElementUI)
Vue.prototype.$http = service
Vue.prototype.$md5 = md5;


const Info = {
  username:'',
  nickname:'',
  email:'',
  user_pic:'',
  power:1,
  pagePower:[],
  token:''
};
const userInfo = localStorage.getItem('userInfo')
if(!userInfo) {
  Vue.prototype.$userInfo = Info
  localStorage.setItem('userInfo',escape(JSON.stringify(Info)))
  Vue.prototype.$user_pic = ''
  localStorage.setItem('user_pic','')
}else{
  Vue.prototype.$userInfo = JSON.parse(unescape(userInfo))
  Vue.prototype.$user_pic = localStorage.getItem('user_pic')
}
sessionStorage.setItem('editID','')
router.beforeEach((to,from,next) =>{
  Vue.prototype.$user_pic = localStorage.getItem('user_pic')
  if (to.path === '/client/clientHome') return next()
  if (to.path === '/') return next('/client/clientHome')
  if (to.path === '/record') return next()
  if (to.path === '/client/lookNews') return next()
  if (to.path === '/client/newpneumonia') return next()

  const tokenStr = JSON.parse(unescape(window.localStorage.getItem('userInfo')))
  if (tokenStr.power !== 1) return next('/record')
  next()
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
