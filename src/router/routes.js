import CLIENT from './client'
import MANAGEMENT from './management'

export default [{
    path: '*',
    component: () => import('../page/client/clientHome.vue')
    },
    ...CLIENT, // 新闻网路由
    ...MANAGEMENT,// 后台管理路由
]