const MANAGEMENT = [{ 
    name: 'management',
    path: '/management/home',
    component: () => import('../page/management/managementHome.vue'),
    children: [
        {
        name: 'newsManage',
        path: '/management/newsManage',
        meta: {
          keepAlive: true,
          headTab: true,
          label: '新闻查询'
        },
        component: () => import('../page/management/newsManage.vue')
      },
      {
        name: 'newsAdd',
        path: '/management/newsAdd',
        meta: {
          keepAlive: true,
          headTab: true,
          label: '添加新闻'
        },
        component: () => import('../page/management/newsAdd.vue')
      },
      {
        name: 'newsPush',
        path: '/management/newsPush',
        meta: {
          keepAlive: true,
          headTab: true,
          label: '新闻发布'
        },
        component: () => import('../page/management/newsPush.vue')
      },
      {
        name: 'newsDelete',
        path: '/management/newsDelete',
        meta: {
          keepAlive: true,
          headTab: true,
          label: '新闻删除'
        },
        component: () => import('../page/management/newsDelete.vue')
      },
      {
        name: 'menberPower',
        path: '/management/menberPower',
        meta: {
          keepAlive: true,
          headTab: true,
          label: '权限管理'
        },
        component: () => import('../page/management/menberPower.vue')
      },
      {
        name: 'newsDynamic',
        path: '/management/newsDynamic',
        meta: {
          keepAlive: true,
          headTab: true,
          label: '新闻动态'
        },
        component: () => import('../page/management/newsDynamic.vue')
      }
    ]
},
{
  name: 'record',
  path: '/record',
  component: () => import('../page/signIn/record.vue'),
}]

export default MANAGEMENT