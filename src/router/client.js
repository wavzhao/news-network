const CLIENT = [{
    name: 'client',
    path: '/client/home',
    component: () => import('../page/client/clientHome.vue'),
    children: [{
        name: 'clientHome',
        path: '/client/clientHome',
        meta: {
          label: '主页'
        },
        component: () => import('../page/client/clientHome.vue')
      }
    ]
},
{
  name: 'lookNews',
  path: '/client/lookNews',
  meta: {
    label: '新闻页面'
  },
  component: () => import('../page/client/lookNews.vue')
},
{
  name: 'newpneumonia',
  path: '/client/newpneumonia',
  meta: {
    label: '疫情专题'
  },
  component: () => import('../page/client/newpneumonia.vue')
}]

export default CLIENT